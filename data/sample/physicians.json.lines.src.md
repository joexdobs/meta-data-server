# Physicians sample data

To save Bitbucket space and to prevent our repository updates from taking a very long time you must down load the 300 megbyte files [physicians.json.lines.zip](https://www.dropbox.com/s/hof2d78sfb1c9oh/physicians.json.lines.zip?dl=1) from dropbox to this directory  data/sample and unzip so you have the file data/sample/physicians.json.lines.txt relative to the repository base.

This file contains JSON records for over 1 million physicians working in the USA.  It was downloaded from a Medicaid site and reformatted into this file format using a python utility.    The format is  id=JSON Blob\n where each records starts with a ID separated from a blob by "=" and the blob is terminated with a "\n"

The file is read and sent to the MDS server one record at a time using [EXLoadPhysicians.go](../../src/EXLoadPhysicians.go)  This utility uses GO concurrency to send many single record requests  to the server simultaneously.   

*  Windows Note: 

  *  I have had some problems with Windows not releasing socket handles and causing error messages but this can be solved by adding an exclusion for the  data/mds to windows defender and adding both mds and EXLoadPhysicians to excluded processes.  
  * Both mds and EXLoadPhysicians should be added as excluded processes under Windows defender or Windows defender can consume over 30% of available CPU inspecting the network traffic between the processes.
  * The directory data should be excluded for search indexing or the search indexer can consume over 10% of available CPU trying to index the files.    
    * Under windows 10.  
      * Enter "Indexing Options" in the search bar 
      * then select "indexing options control panel" 
      * then  click "Modify" button.  
      * Then Drill down to parent directory click on it 
      * then you can unclick the child directory such as "data" that should not be indexed and they will be marked as excluded.
    * ​

  ​