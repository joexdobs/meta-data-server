package main

//"encoding/json"

import (
	"fmt"
	//"io"
	//"io/ioutil"
	"log"
	"net/http"
	//"net/url"
	//"os"
	//"os/exec"
	"mds"
	"strings"
)

type StaticFileDesc struct {
	FiName string
}

// Serve static file where the same file name is served every time
// This is used to allow safely serving some files outside our
// normally protected directory structure without having creating
// too large of a security overead.
func (v StaticFileDesc) ServeStaticFile(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, v.FiName)
}

/* Simple handler to demonstrate responding to
a posted form.   Can be accessed via:
http://127.0.0.1:9601/ping?name=Joe&handle=workinghard
values will print to console. */
func ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "text/plain")
	r.ParseForm() // parse arguments, you have to call this by yourself
	w.WriteHeader(http.StatusOK)
	fmt.Println(r.Form) // print form information in server side
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println("query", r.URL.Query())
	fmt.Println("url ", r.URL.RequestURI())
	fmt.Println(r.Form["url_long"])
	fmt.Fprintf(w, "printFormVal\n url=%s\n", r.URL.Path) // send data to client side
	for k, v := range r.Form {
		fmt.Println("key:", k, "\t", " val:", strings.Join(v, ""))
		fmt.Fprintf(w, "key:%s\t val=%s\n", k, strings.Join(v, ""))
	}
}

//func drains_elev_handler(w http.ResponseWriter, r *http.Request) {
//	simplePythonCGI(w, r, "C:\\jscala\\dem\\web\\doc", "drains_elev.svc")
//}

/*func downloadHandler(w http.ResponseWriter, r *http.Request) {
        r.ParseForm()
        StoredAs := r.Form.Get("StoredAs") // file name
        data, err := ioutil.ReadFile("files/"+StoredAs)
        if err != nil { fmt.Fprint(w, err) }
        http.ServeContent(w, r, StoredAs, time.Now(),   bytes.NewReader(data))
}*/

func main() {
	//http.Handle("/gen/", http.FileServer(http.Dir("data/gen")))
	http.Handle("/csv/", http.FileServer(http.Dir("/joe/git/CSVTablesInBrowser")))
	http.HandleFunc("/readme.md", StaticFileDesc{"README.md"}.ServeStaticFile)

	http.Handle("/", http.FileServer(http.Dir("docs")))
	http.Handle("/documentation/", http.StripPrefix("/documentation/", http.FileServer(http.Dir("documentation"))))
	http.Handle("/data/", http.StripPrefix("/data/", http.FileServer(http.Dir("data"))))
	http.Handle("/src/", http.StripPrefix("/src/", http.FileServer(http.Dir("src"))))

	http.HandleFunc("/mds/", mds.HandlerMDS)

	// When path ends with "/" it is treated as a tree root
	// which allos the handler to pick up the path and any
	// sub paths such as /ping/apple.
	http.HandleFunc("/ping/", mds.Ping)      // set router
	err := http.ListenAndServe(":9601", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
