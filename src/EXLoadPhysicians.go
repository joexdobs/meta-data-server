package main

import (
	"fmt"
	//"io"
	//"io/ioutil"
	//"log"
	"net/http"
	//"net/url"
	"os"
	//"os/exec"
	//"mds"
	"bufio"
	"bytes"
	//"runtime"
	"mdsClient"
	"strings"
	"time"
)

type Updater struct {
	numReq            int
	numSuc            int
	numFail           int
	startTime         time.Time
	numSinceStatPrint int
	printEvery        int
}

func keepLines(s string, n int) string {
	result := strings.Join(strings.Split(s, "\n")[:n], "\n")
	return strings.Replace(result, "\r", "", -1)
}

// https://dlintw.github.io/gobyexample/public/http-client.html

func (u *Updater) PrintStat() {
	elapSec := time.Since(u.startTime).Seconds()
	reqPerSec := float64(u.numReq) / float64(elapSec)
	failRate := u.numFail / u.numReq
	fmt.Println("numReq=", u.numReq, "elapSec=", elapSec, "numSuc=", u.numSuc, "numFail=", u.numFail, "failRate=", failRate, "reqPerSec=", reqPerSec)
	u.numSinceStatPrint = 0
}

func (u *Updater) CheckAndPrintStat() {
	if u.numSinceStatPrint >= u.printEvery {
		u.PrintStat()
	}
}

func (u *Updater) procLine(baseURI string, aline string, idFile *os.File) {
	//now := time.Now().UTC()
	arr := strings.SplitN(aline, "=", 2)
	u.numReq += 1

	if len(aline) < 1 || len(arr) < 2 {
		return
	}
	//fmt.Println("aline=", aline)
	postStr := arr[1]
	id := arr[0]
	postBody := []byte(postStr)
	uri := "http://127.0.0.1:9601/mds/" + id
	// Separate ID from Line and write to disk
	fmt.Fprintf(idFile, "%s\n", id)

	hc := http.Client{}
	req, err := http.NewRequest("PUT", uri, bytes.NewBuffer(postBody))
	if err != nil {
		fmt.Println("error opening uri=", uri, " err=", err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("meta-id", id)
	req.Header.Set("Connection", "close")
	req.Close = true
	resp, err := hc.Do(req)
	if err != nil {
		fmt.Println("err=", err, "resp=", resp)
		return
	}
	defer resp.Body.Close()
	// TODO: Check response to
	//  determine if error
	//  update counts

	//fmt.Println(" reps=", resp, "err=", err)
	//body, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode == 200 {
		u.numSuc += 1
	} else {
		u.numFail += 1
	}
	u.numSinceStatPrint += 1
	u.CheckAndPrintStat()

	//fmt.Println(" body=", string(body))
	//len(body)
	//defer mdsClient.TimeTrack(now, "finished id=" + id)
	time.Sleep(1500)

}

var (
	server *http.Server
	client *http.Client
)

func main() {
	const procs = 2
	const MaxWorkerThread = 8 //5 //15 // 50 // 350
	const MaxQueueSize = 3000
	const BaseURI = "http://127.0.0.1:9601/mds/"
	sourceFiName := "data/sample/physicians.json.lines.txt"
	idFiName := "data/sample/physicians.id.txt"
	fmt.Println("source = ", sourceFiName, " baseURI=", BaseURI)
	fmt.Println("MaxWorkerThread=", MaxWorkerThread)
	fmt.Println("MaxQueueSize=", MaxQueueSize)
	u := Updater{startTime: time.Now().UTC(), printEvery: 100000}

	start := time.Now().UTC()
	file, err := os.Open(sourceFiName)
	if err != nil {
		fmt.Println("error opening meta file ", sourceFiName, " err=", err)
	}
	defer file.Close()

	var idFile, idErr = os.Create(idFiName)
	if idErr != nil {
		fmt.Println("Can not open meta file ", idFiName, " idErr=", idErr)
		return
	}

	defer idFile.Close()

	//header := w.Header()
	scanner := bufio.NewScanner(file)
	linesChan := make(chan string, MaxQueueSize)
	done := make(chan bool)

	//client = &http.Client{
	//	Transport: &http.Transport{
	//		// just what default client uses
	//		Proxy: http.ProxyFromEnvironment,
	//		// this leads to more stable numbers
	//		MaxIdleConnsPerHost: procs * runtime.GOMAXPROCS(0),
	//	}
	//	}

	//tr := &http.Transport{
	//	MaxIdleConns:        10,
	//	IdleConnTimeout:     30 * time.Second,
	//	DisableCompression:  true,
	//	MaxIdleConnsPerHost: procs * runtime.GOMAXPROCS(0),
	//}
	//client = &http.Client{Transport: tr}

	// Spin up 100 worker threads to post
	// content to the server.
	for i := 0; i < MaxWorkerThread; i++ {
		go func() {
			for {
				aline, more := <-linesChan
				if more {
					u.procLine(BaseURI, aline, idFile)

				} else {
					done <- true
					return
				}
			}
		}()
	}

	// Add the rows to the Queue
	// so we can process them in parralell
	// It is blocked at MaxQueueSize by the
	// channel size.
	for scanner.Scan() {
		aline := scanner.Text()
		//for len(linesChan) > (MaxQueueSize - 5) {
		//	time.Sleep(200)
		//}

		if len(aline) > 0 {
			linesChan <- aline
		}

	}
	close(linesChan)
	fmt.Println("Queued all jobs")
	mdsClient.TimeTrackMin(os.Stdout, start, "finished Queing")
	<-done // wait until queue has been marked as finished.
	fmt.Println("Sent All Jobs")
	mdsClient.TimeTrackMin(os.Stdout, start, "inserted all records")
	u.PrintStat()
}
