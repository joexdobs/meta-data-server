package mds

import (
	//"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	//"log"
	//"net/url"
	"os"
	//"os/exec"
	"bufio"
	"path"
)

const MDSURIPrefix = "/mds/"
const MDSDataPrefix = "../mds-data/mds/"

func SaveReqMetaHeaders(fiName string, r *http.Request) {
	var mfile, err = os.Create(fiName)
	if err != nil {
		fmt.Println("Can not open meta file ", fiName, " err=", err)
		return
	}
	defer mfile.Close()
	// Always Write content-type first
	// because we want to set it first when
	// reading the content.
	contentType := r.Header.Get("content-type")
	if contentType > " " {
		fmt.Fprintf(mfile, "content-type=%s\n", contentType)
	}
	for mname, headers := range r.Header {
		mname = strings.ToLower(mname)
		if strings.HasPrefix(mname, "meta-") {
			for _, h := range headers {
				fmt.Fprintf(mfile, "%s=%s\n", mname, h)
			}
		}
	}
}

func SendSavedHeaders(fiName string, w http.ResponseWriter) {
	file, err := os.Open(fiName)
	if err != nil {
		fmt.Println("error opening meta file ", fiName, " err=", err)
	}
	defer file.Close()
	header := w.Header()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		aline := scanner.Text()
		arr := strings.SplitN(aline, "=", 2)
		if len(aline) > 0 && len(arr) > 1 {
			mkey := arr[0]
			mval := arr[1]
			header.Set(mkey, mval)
		}
	}
}

/* Save a local file in directory system using 3 letter
prefix. Adds meta data if present.  Also saves content type
as file extension if known */
func SaveMDSFile(w http.ResponseWriter, r *http.Request) {
	//fmt.Println("SaveMDSFile", r.URL.Scheme, "method=", r.Method)
	//fmt.Println("URL=", r.URL)
	var urstr = r.URL.RequestURI()
	id := GetIdFromURI(urstr, MDSURIPrefix)
	//fmt.Println("id=", id)
	if len(id) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "id is mandatory as last segment of URI")
		return
	}
	// Read the body string.
	//fmt.Println("contentLength=", r.ContentLength)
	bodya := make([]byte, r.ContentLength)
	_, err := r.Body.Read(bodya)

	outFiName := GetPathFromID(id, MDSDataPrefix)
	dirName := path.Dir(outFiName)
	EnsurDir(dirName)
	//fmt.Println("save finame=", outFiName)

	err = ioutil.WriteFile(outFiName, bodya, 0644)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "err saving file ", err)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "{'status' : 'sucess'}")

	// Now Write the Metadata file by looping
	metaFileName := strings.Replace(outFiName, "FIL", "M.TXT", -1)
	SaveReqMetaHeaders(metaFileName, r)
	//bodys := string(bodya)
	//fmt.Println("bodys=", bodys)
}

func ReadMDSFile(w http.ResponseWriter, r *http.Request) {
	//fmt.Println("ReadMDSFile", r.URL.Scheme, "method=", r.Method)
	//fmt.Println("URL=", r.URL)
	var urstr = r.URL.RequestURI()
	id := GetIdFromURI(urstr, MDSURIPrefix)
	//fmt.Println("id=", id)
	if len(id) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "id is mandatory as last segment of URI")
		return
	}

	fiName := GetPathFromID(id, MDSDataPrefix)
	//fmt.Println("finame=", fiName)
	if Exists(fiName) {
		metaFileName := strings.Replace(fiName, ".FIL", ".M.TXT", -1)
		SendSavedHeaders(metaFileName, w)
		inFi, err := os.Open(fiName)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			//fmt.Println("fail attempt to open ", fiName, "err=", err)
			//fmt.Fprintf(w, "failed to open %s with err %s", fiName, err)
			return
		}

		defer inFi.Close()
		statinfo, err := inFi.Stat()
		w.Header().Set("Last-Modified", statinfo.ModTime().Format(http.TimeFormat))
		w.WriteHeader(http.StatusOK)

		_, err = io.Copy(w, inFi)
		if err != nil {
			//w.WriteHeader(http.StatusInternalServerError)
			fmt.Println("failed attempt to copy file ", fiName, " to http stream")
			return
		}

	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "404 not found")
		//fmt.Println("cound not find file", fiName)
		return
	}
}

func DeleteMDSFile(w http.ResponseWriter, r *http.Request) {
	//fmt.Println("DeleteMDSFile", r.URL.Scheme, "method=", r.Method)
	//fmt.Println("URL=", r.URL)
	var urstr = r.URL.RequestURI()
	id := GetIdFromURI(urstr, MDSURIPrefix)
	//fmt.Println("id=", id)
	if len(id) < 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "id is mandatory as last segment of URI")
		return
	}
	fiName := GetPathFromID(id, MDSDataPrefix)
	//fmt.Println("finame=", fiName)
	if Exists(fiName) {
		var err = os.Remove(fiName)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Println(" remove file failed ", fiName, " err=", err)
		} else {
			w.WriteHeader(http.StatusOK)
		}

	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "404 not found")
		//fmt.Println("cound not find file", fiName)
		return
	}

	metaFileName := strings.Replace(fiName, ".FIL", ".M.TXT", -1)
	if Exists(metaFileName) {
		os.Remove(metaFileName)
	}

}

func HandlerMDS(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		ReadMDSFile(w, r)
	} else if r.Method == "PUT" {
		SaveMDSFile(w, r)
	} else if r.Method == "DELETE" {
		DeleteMDSFile(w, r)
	} else {
		//fmt.Println("Bad VERB")
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "method %s is not supported ", r.Method)
	}
}
