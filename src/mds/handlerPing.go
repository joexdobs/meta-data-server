package mds

import (
	"fmt"
	//"io"
	//"io/ioutil"
	//"log"
	"net/http"
	//"net/url"
	//"os"
	//"os/exec"
	"strings"
    
)

/* Simple handler to demonstrate responding to
a posted form.   Can be accessed via:
http://127.0.0.1:9601/ping?name=Joe&handle=workinghard
values will print to console. */
func Ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "text/plain")
	r.ParseForm() // parse arguments, you have to call this by yourself
	w.WriteHeader(http.StatusOK)
	fmt.Println(r.Form) // print form information in server side
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println("query", r.URL.Query())
	fmt.Println("url ", r.URL.RequestURI())
	fmt.Println(r.Form["url_long"])
	fmt.Fprintf(w, "printFormVal\n url=%s\n", r.URL.Path) // send data to client side
	for k, v := range r.Form {
		fmt.Println("key:", k, "\t", " val:", strings.Join(v, ""))
		fmt.Fprintf(w, "key:%s\t val=%s\n", k, strings.Join(v, ""))
	}
}
