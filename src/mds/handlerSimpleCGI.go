package mds

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	//"log"
	//"net/url"
	"os"
	"os/exec"
)

// Generalized handler to process basic Python
// CGI calls passing the relative values to the
// python through CGI enviornment variables.
// reading result from stdout.   Use this as
// to allow local testing of CGI scripts from
// inside the GO enviornment.
func SimplePythonCGI(w http.ResponseWriter, r *http.Request, cwd string, scriptPath string) {
	var urstr = r.URL.RequestURI()
	var ursplt = strings.SplitN(urstr, "?", 2)
	//fmt.Println("ursplt=", ursplt)
	var queryPart = ""
	if len(ursplt) == 2 {
		queryPart = ursplt[1]
	}
	//fmt.Println("queryPart=", queryPart)

	binary, err := exec.LookPath("python.exe")
	if err != nil {
		// Need to detect error and send back server error
		// code.
		//return err
	}
	fmt.Println("python path", binary)

	cmd := exec.Command(binary)
	env := os.Environ()
	env = append(env, fmt.Sprintf("QUERY_STRING=%s", queryPart))
	// TODO:  Add the other things like method, remote client IP, cookies, Etc
	//     so they comply with the basic CGI expectations

	cmd.Env = env
	cmd.Dir = cwd
	cmd.Args = append(cmd.Args, scriptPath)

	cmdOut, _ := cmd.StdoutPipe()
	//cmdErr, _ := cmd.StderrPipe()

	startErr := cmd.Start()
	if startErr != nil {
		// Need to detect error and send back server error
		// code.
		//return startErr
	}

	// read stdout and stderr
	stdOutput, _ := ioutil.ReadAll(cmdOut)
	// note would be faster and less memory
	// to parse the headers out by leaving everything
	// in byte format.
	fmt.Printf("STDOUT: %s\n", stdOutput)
	//errOutput, _ := ioutil.ReadAll(cmdErr)

	stdostr := strings.Replace(strings.Replace(string(stdOutput), "\r\r", "\r", -1), "\n\n", "\n", -1)
	// Needed this because for some reason my python output
	// was arriving with extra \r even though the code didn't
	// generate them. Think it is in the windows pipe.

	// Have to take the first Lines Response Code, Status Content type
	//  parse them out and use for the writer response.
	// w.Header().Set("Content-Type", "text/plain")
	//func Error(w ResponseWriter, error string, code int)
	//fmt.Printf("stdostr: %s\n", stdostr)
	resparr := strings.SplitN(stdostr, "\r\n\r\n", 2)
	headarr := strings.Split(resparr[0], "\r\n")
	//fmt.Printf("headarr: %s\n", headarr)
	//fmt.Printf("resparr len=%d: val%s\n", len(resparr),resparr)
	for _, hval := range headarr {
		//fmt.Printf(" hndx=%d hval=%s\n", hndx, hval)
		hvalarr := strings.SplitN(hval, ":", 2)
		if len(hvalarr) > 1 {
			w.Header().Set(hvalarr[0], hvalarr[1])
		}
	}

	body := resparr[1]
	io.WriteString(w, body)
	//fmt.Printf("STDOUT: %s\n", stdOutput)
	//fmt.Printf("ERROUT: %s\n", errOutput)

	err = cmd.Wait()
}
