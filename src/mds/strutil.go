package mds

import (
	"bytes"
	"fmt"
	//"net/http"
	"strings"
	//"io"
	//"io/ioutil"
	//"log"
	"net/url"
	"os"
	//"os/exec"
)

// https://golang.org/doc/articles/wiki/
// https://elithrar.github.io/article/custom-handlers-avoiding-globals/

// Take arbitrary length ID and
// break it into 3 character segments
// separated by path to allow us to create
// sub directories to track it.
func ConvNameToPath(strIn string, maxSegLen int) string {
	var buff bytes.Buffer
	ctr := 0
	//slen := len(strIn)
	for _, d := range strIn {
		if ctr >= maxSegLen {
			//if ndx < slen {
			buff.WriteByte('/')
			ctr = 0
			//}
		}
		buff.WriteByte(byte(d))
		ctr += 1
	}
	return buff.String()
}

// Map Bad characters we do not want in
// our file names into their ASII number
// equivelants.
func MapBadIDChar(strIn string) string {
	var buff bytes.Buffer
	slower := strings.ToLower(strIn)
	for _, d := range slower {
		// if character outside range of 0..9, a..z, -_ then force conversion
		// to ascii number value.
		if (d >= '0' && d <= '9') || (d >= 'a' && d <= 'z') || d == '.' || d == '-' || d == '_' {
			buff.WriteByte(byte(d))
		} else {
			buff.WriteString(fmt.Sprintf("%%%X", d))
		}
	}
	return buff.String()
}

func Exists(filePath string) bool {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}

func MakeDirName(baseDir string, id string) string {
	// newpath := filepath.Join(".", "public")
	//return dirName
	return "data/xyz"
}

func EnsurDir(dpath string) {

	if _, err := os.Stat(dpath); os.IsNotExist(err) {
		os.MkdirAll(dpath, os.ModePerm)
	}

}

func GetIdFromURI(urstr string, prefix string) string {

	id := strings.Replace(urstr, prefix, "", 1)
	//fmt.Println("id =", id)
	id = strings.SplitN(id, "?", 2)[0]
	id, _ = url.QueryUnescape(id)
	//id = strings.Replace(id, "\\", "-", -1)
	//id = strings.Replace(id, "/", "-", -1)
	//id = strings.Replace(id, " ", "-", -1)
	//id = strings.Replace(id, ":", "-", -1)
	return id
}

func GetPathFromID(id string, prefix string) string {
	idadj1 := MapBadIDChar(id)
	//fmt.Println("idadj1=", idadj1)
	idadj2 := ConvNameToPath(idadj1, 5)
	//fmt.Println("idadj2=", idadj2)
	outFiName := prefix + idadj2 + ".FIL"
	return outFiName
}
