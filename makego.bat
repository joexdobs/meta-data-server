set GOPATH=%cd%
::  Run get-dependencies.bat before running this
::  script.
go get "github.com/lestrrat/go-apache-logformat"
go build src/mds.go
go build src/EXREadSingleRecord.go
go build src/EXSaveSingleRecord.go
go build src/EXReadSingleMultiThreaded.go
go build src/EXDeleteSingleRecord.go
go build src/EXLoadPhysicians.go
:: LoadPhysicians requires data downloaded separately
::  see: data/sample/physicians.json.lines.src.md
go build src/GenericHTTPTestClient.go

