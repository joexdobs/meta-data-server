# TO Actions Meta Data Server

## Technical Enhancements

* Modify HTTP handlers to use new 1.9 syntax for context and extend to properly support timeout especially for post / put operations.

  ​




## Feature Enhancements

* Run on larger server to get current Max performance capacity
  * Test impact of Spawning multiple processes to better utilize multi-core computers with less thread context switching.  Best guess is 3X to 5X better read performance with 16 cores on well cached data but need to test.
  * Linux configuration script to start with base centos and install all pre-requisites,  download repository and start the service.   Also a update version to update to most current version. Make sure credentials are passed in and environment variables or command line parameters
  * Need to modify tester to run requests against multiple listen ports to measure impact from multiple servers.
* Add Wiki / Linked in Article showing how to use. 
* Read config values from JSON or INI file or CMD line
  * Add listen port -listen cmd line parameter
  * Add data directory -dataDir as cmd line parameter
  * Add docs directory -docs as cmd line parameter
  * Add maxWriteThreads -maxWriteThread as cmd line parameter
  * Add maxReadThreads -maxReadThread as cmd line parameter
  * Add optional config setting to encrypt files as written to disk
  * Add optional config setting to compress files as written to disk.  Since we know that minimum file size is 4K we only compress those greater than 4K.
  * Add -cert cmd line parameter to name certificate file for https
* Add thread safe logging with buffer swap.
* Add thread control using a write queue and read queue to control maximum demands placed on server.
* Add support for multi-file GET
  * Measure performance from Mutli-file get.
  * Write a python utility to generate multi-file get test requests using random selection from set.
* Add ability to retrieve users from data/users directory where metadata for roles is retrieved. 
* Add per request logging in the desired form as a sample
* Add HTTPS easily enabled from configuration
* Add first level index name as first path after /mds then allow each index to have a separate mount point.  That will at least allow us to spread separate indexes across different drives.
* For the "/doc" directory recognize .md files and render markdown as HTML
* Add multi-GET per single request to server 
  - Add multi threaded test client for multi get
* Add multi-PUT per single PUT request to server 
  - Add multi-threaded test client for Multi PUT
  - Add support for URL encoded metadata as part of PUT and _bulk service.  This is needed to allow each document to store a title, author and a list of roles required for access. 
  - When retrieving or saving multiple documents can not just spawn the total list of file saver go routines because if we had dozens of callers each sending in hundred item fetches we would exceed the max # of open files.   We have to limit the number of connections used by any single client and also limit to keep total set of connections below the maximum allowed.
  - Server needs to span a number of GO routines that represents maximum file read / write handlers then send process requests for file read, write for the underlying requests to these.  That way if we get 10,000 inbound connections we will not violate server max open file handles limit. 
* Read document contents from Kafka queue.
* Performance
  * Add a bash script to fully provision a Linux Centos from Boot to running repository
  * Run Test on larger computer where we have at least 16 cores.  See how performance degrades when all cores are busy.
  * Run Test on larger computer with multi-threaded get running on separate computers.
  * Add Provider search to leased computer
  * Lease server with large # core to test performance
* Use GO Profiler to find hot spots when under heavy PUT activity see what I can do to improve.

* Use GO Profiler to find hot spots when under heavy GET activity and see what I can do to improve.
* Security

  * Add ability to receive open ID user identity and roles from open ID.  User must contain all roles defined for the document to allow access.
  * Add log of document access.
  * Add ability to filter content availability by user role.
  * Add ability to index and search with elastic search items in the metadata store.
  * Add retrieval of encryption key from vault or from config file.   Encrypt file contents on disk with contents from vault but ensure it uses the intel encryption hardware
* Search & Indexing
  * Consider adding a splay disk tree by token to easily locate items by a set of tokens.  As alternative to installing full elastic search.
  * Index Metadata in elastic search to allow fast access via search.  Will need to convert meta-keywords into array.


- Consider adding records to flat file with Key  values indexed with a BTree  byte offset is the value. This should offer greater performance compared to the file based system.

- Find a larger sample data set for testing

- Add ability to compress and / or encrypt data for only specific prefixes.

- Add test client setting to measure impact of compression and encryption settings on disk

- Investigate building shared libraries as separate src repository that can be imported rather than embedding.

- Add test client storing JPG images from sample src directory

- Allow more than one path to be defined for different content.  Spread content evenly across drives.
  - Spread content evenly across drives.
  - Allow a mechanism to spread keys uniquely across drives.
  - provide mechanism to spread content across drives to distribute seeks evenly across drives.  This should provide better speed scalability if all heads can seek independently.

- ​

- Distributed Nodes
  - Support automatic propagation of content to other nodes.
  - Support key space sharding across multiple storage mount points.
  - Support key space sharding across multiple distributed nodes.  Also support automatic distribution across supported nodes by available space. 

- ​

- When deleting a file it currently leaves a empty directory when removing the file if no other files are present in that directory.  It should walk up the directory tree and remove directories that would otherwise be empty.  Otherwise will leave a bunch of  disk storage used.

- Add test add logic to ramp up concurrent PUT adds to find the sweet spot for maximum concurrent connections.

- Add test logic to GET to ramp up concurrent adds to find sweet spot for maximum concurrent reads per second.

- ADMIN
  - Delete all documents with ID starting with a specific prefix.
  - Return space available on available drives.



## Potential Larger Data Sources

  - Download additional test data sets.  We need at least a few T to ensure we are properly exercising cache misses on a 96GB server.

    - [us government downloadable data index](https://catalog.data.gov/dataset)
    - [HUD housing survey affordability](https://www.huduser.gov/portal/datasets/hads/hads.html)
    - [NOAA hourly rainfall records](https://catalog.data.gov/dataset/u-s-hourly-precipitation-data) [landing page](https://catalog.data.gov/dataset/u-s-hourly-precipitation-data)  [ftp server](ftp://ftp.ncdc.noaa.gov/pub/data/hourly_precip-3240/)  multi megabytes
    - [Nutritiona, physical activity and Obesity behavior surveillance](https://catalog.data.gov/dataset/nutrition-physical-activity-and-obesity-behavioral-risk-factor-surveillance-system)
    - [National survey on Drug use and Health](https://catalog.data.gov/dataset/national-survey-on-drug-use-and-health-nsduh-2015)
    - [National Stock Number Extract](https://catalog.data.gov/dataset/national-stock-number-extract)
    - [USGS small-scale data set cities and towns of united states shape file](https://catalog.data.gov/dataset/usgs-small-scale-dataset-cities-and-towns-of-the-united-states-201403-shapefile)
    - [500 cities local data for better health](https://catalog.data.gov/dataset/500-cities-local-data-for-better-health-b32fd)
    - [Expenditures on children by Families](https://catalog.data.gov/dataset/expenditures-on-children-by-families)
    - [Statisitcal abstract of the united states](https://catalog.data.gov/dataset/statistical-abstract-of-the-united-states)
    - [Food price outlook portion of CPI](https://catalog.data.gov/dataset/food-price-outlook)
    - [Community Health Status Indicators to combat Obesity, Heart Disease and Cancer](https://catalog.data.gov/dataset/community-health-status-indicators-chsi-to-combat-obesity-heart-disease-and-cancer)
    - [Realtime Air quality KML file](https://catalog.data.gov/dataset/airnow-real-time-air-quality-kml-file)
    - [Agency data on user facilities](https://catalog.data.gov/dataset/agency-data-on-user-facilities)
    - [USDA Rural Development resale properties - Foreclosure](https://catalog.data.gov/dataset/usda-rural-development-resale-properties-foreclosure)
    - [U.S. Daily Climate Normals through 2010](https://catalog.data.gov/dataset/u-s-daily-climate-normals-1981-2010) [source ftp](ftp://ftp.ncdc.noaa.gov/pub/data/normals/1981-2010/source-datasets/) At least 6 GB of data in GZ form.  [file formats](ftp://ftp.ncdc.noaa.gov/pub/data/normals/1981-2010/readme.txt)
    - [storm events database](https://catalog.data.gov/dataset/ncdc-storm-events-database)
    - [National Park Boundaries](https://catalog.data.gov/dataset/national-park-boundariesf0a4c) 
    - [Farmers Markets Directory and Geographic Data](https://catalog.data.gov/dataset/farmers-markets-geographic-data)
    - [Deaths in US Cities 1962 to 2016](https://catalog.data.gov/dataset/deaths-in-122-u-s-cities-1962-2016-122-cities-mortality-reporting-system)
    - [National Land cover database](https://catalog.data.gov/dataset/national-land-cover-database-nlcd-land-cover-collection)
    - [Storm prediction report](https://catalog.data.gov/dataset/storm-prediction-report)
    - [2015 shape file US census by Zipcode](https://catalog.data.gov/dataset/tiger-line-shapefile-2015-2010-nation-u-s-2010-census-5-digit-zip-code-tabulation-area-zcta5-na)  [underlying data](https://catalog.data.gov/dataset/tiger-line-shapefile-2015-2010-nation-u-s-2010-census-5-digit-zip-code-tabulation-area-zcta5-na)
    - [United States Average Annual Precipitation Direct Download](https://catalog.data.gov/dataset/united-states-average-annual-precipitation-1990-2009-direct-download)
    - [Heart Disease mortality data among US adults 35+ by State](https://catalog.data.gov/dataset/heart-disease-mortality-data-among-us-adults-35-by-state-territory-and-county-5fb7c)
    - ​

    ​



# DONE

- DONE:2017-11-05:JOE: Make readme.md available from main server
- DONE:2017-11-05:JOE: Add index file to main server with some links to help user find the bitbucket repository
- DONE:JOE:2017-10-16: Add GenericHTTPTestClient to make testing various scenarios easy without writing additional test driver code.
- DONE:JOE:2017-10-09: Add multi threaded test client for single get.
- DONE:JOE:2017-10-09: Update single file PUT,  GET,  DELETE documentation to reflect current API implementation.
- DONE:JOE:2017-10-09: Add timing logic to test client.  Also add detect of failure and counts of success versus failure.   Measure complete add time.
- DONE:JOE:2017-10-09Add test client logic to harvest ID and save it to a separate file for use in read test.
- DONE: Add large file support save to test client.-DONE:JWORK:2017-10-07: Add support for last update time 
- -DONE:JOE:2017-10-07: For single file MDS look for cookies with prefix of META and save them in the file META
- -DONE:JOE:2018-10-07: Set Date header to represent file last change time on GET operations.
- -DONE:JOE:2017-10-07:Add ability to return Metadata when &metadata=true
- -DONE:JOE:2017-10-07:Add ability to save Metadata when set 
- -DONE:JOE:2017-10-07:Store content type as part of metadata