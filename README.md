# Meta Data Server in GO #

By Joe Ellsworth Jan-2017 All rights reserved.    Feel free to contact me for a liberal commercial use license.     Personal use is free under a MIT license.

Mange, store, retrieve  serialized metadata by ID.    Also provides API to save or retrieve metadata for multiple documents in a single HTTP call to reduce serial latency.     

This service is designed to manage many terabytes of document data such as product descriptions,  member detail cost effectively.  The most common use case is to store complex JSON documents that are expensive to retrieve from a RDBMS system because they are composed of data from many tables.  By storing this data in pre-serialized form many processes can retrieve the required data much faster than it could be pulled from the RDBMS.  I use this to inexpensively provide horizontal scalability for read centric applications.    A primary advantage is that by shifting load on the RDBMS for read centric calls to far less expensive hardware it allows RDBMS scale to support more business. 

**Live Demo Server:  [demo.BayesAnalytic.com:9601](http://demo.BayesAnalytic.com:9601)**

I designed this to support a "Shared Nothing" or toaster style architecture that provides scalability and stability by duplicating inexpensive Toasters.  It is predicated on using the least expensive servers capable of storing the data so it is cost effective to scale by adding additional nodes.    In a shared nothing architecture the minimum number of nodes should be 3 which allows one node to be taken down for service and still be two failures away from a sev-1 outage.

When deployed in a Shared Nothing architecture each node will store a complete copy of the documents.   It is most cost effective to deploy without Raid mirroring because it it generally cheaper to spin up a extra node to provide redundancy than it is to use High Availability hardware.   Typically deploy with drives configured as RAID 5 to allow up to two drives to fail and be replaced with hot swapping but it is equally valid to use cheaper servers with fixed drives and run a couple extra nodes in lieu of the more expensive hot swap drives.

#### Author & License

Joseph Ellsworth  CTO of Bayes Analytic LLC.  Written Jan-2017 adapted from a earlier design in Node.js written during 2014.

## Sample / Test Code:

- [**src/EXSaveSingleRecord.go**](src/EXSaveSingleRecord.go) - Save a single record using web service API into the MDS repository
- [**src/EXReadSingleRecord.go**](src/EXReadSingleRecord.go) - Read a single record using web service API from the repository
- [**src/EXDeleteSingleRecord.go**](src/EXDeleteSingleRecord.go)  - Delete a single record using web service API from the repository
- [**src/EXLoadPhysicians.go**](src/EXLoadPhysicians.go) - Loads about 1.2 million records from data/sample/physicians.json.lines.txt into the repository.   Runs 50 concurrent threads simultaneously and reports performance as it runs.   The source data must be downloaded from Dropbox See:  [data/sample/physicians.json.lines.src.md](data/sample/physicians.json.lines.src.md) for instructions.   This utility can be easily changed to test different thread configurations to find what is optimal for the local machine.
- [**src/EXReadSingleMultiThreaded.go**](src/EXReadSingleMultiThreaded) - Read the list of ID from data/..... and submits them to the server in a multi-threaded fashion measuring performance.  Designed to help determine read capability of server under heavy read load.
- [**src/GenericHTTPTestClient.go**](src/GenericHTTPTestClient.go) - [docs](src/GenericHTTPTestClient.md) - Reads JSON test specs from a data file,  executes them against a server and prints out success, failure and timing data.  This code is intended to avoid the need to write specific HTTP tests when building out basic test scenarios for various servers.

## API Documentation

### Read single document

Note: The repository starts empty.  You must run some of the update samples before the GET or read operations can succeed.    

* The default server 127.0.0.1:9601 loops back to your local machine for the service.  This can be adjusted with a specific server name such as demo.BayesAnalytic.com:9601

    HTTP Verb: GET
    URI:  /mds/{key"}
    RESPONSE:
      200 if document found 
      404 if document not found
      *TODO: 401 if user / role is not allowed access to this document
      *TODO: 403 if insufficient user credentials supplied.
      
    During 200 response the HTTP document body will contain the bytes
    originally stored for that document.

>>>#### SAMPLES:  
>>>
>>>Browser:  
>>>
>>>​        http://127.0.0.1:9601/mds/1918171
>>>
>>>​        [http://demo.BayesAnalytic.com:9601/mds/1918171](http://demo.BayesAnalytic.com:9601/mds/1918171)
>>>
>>>> ```
>>>> go run src/EXReadSingleRecord.go
>>>>     will show statusCode, body and headers
>>>> ```
>>>
>>>
>>>> ```
>>>> go build src/EXReadSingleMultiThreaded.go
>>>> EXReadSingleMultiThreaded
>>>>     will read a list of ID from source file and
>>>>     submit them as a series of single record fetch
>>>>     displaying performance as it goes. 
>>>> ```
>>>
>>>> ```
>>>> curl http://127.0.0.1:9601/mds/1918171
>>>>     will show the document body
>>>>     
>>>> curl --head -X GET http://127.0.0.1:9601/mds/1918171
>>>>     will show headers but not document body 
>>>>     
>>>> curl -D - -X GET http://127.0.0.1:9601/mds/1918171
>>>>    will show headers including document body
>>>>    
>>>> ```

### Save / Update single document
    Saves a single document into the repository at the specified ID.  This document can
    be retrieved latter by this key.
    HTTP Verb:  PUT
    URI:  /mds/{key}
    example:   PUT  /mds/12835   
       PUT body contains bytes of document to save in server.

>>>#### SAMPLES:  
>>>>
>>>> ```
>>>>go run src/EXSaveSingleRecord.go
>>>> ```
>>>>
>>>> ```
>>>>curl -X PUT "Content-Type: application/json;" -d '{"id" : 1918171, "title", "this is a test structure"}' http://127.0.0.1:9601/mds/1918171 
>>>> ```
>>>>
>>>> ```
>>>>go run src/EXLoadPhysicians.go
>>>>    Uses multi-threading to add many documents at a time
>>>>    using single document PUT calls.  Adds a total of about
>>>>    1 million records.   Reads data from
>>>>    data/sample/physicians.json.lines.txt which must be
>>>>    downloaded and unzipped from dropbox.  See
>>>>    data/sample/physicians.json.lines.src.md
>>>> ```
>>>>

### Delete a single Document
    HTTP Verb: DELETE
    URI:  /mds/{key}
    Example:  DELETE /mds/12835
    
    Returns 200 if sucessful
    Returns 404 if document with that ID does not exist
    
    ...
>>>#### SAMPLES:  
>>>>
>>>> ```
>>>>go run src/EXDeleteSingleRecord.go
>>>> ```
>>>>
>>>> ```
>>>> curl -X DELETE http://127.0.0.1:9601/mds/1918171 
>>>> ```
>>>>
### TODO: Bulk Read many documents in single request:
    HTTP Verb: GET
    URI:  /mdsbulk?id={key1}&id={key2}&id={key3}...&id={keyN}&delim=13
    EXAMPLE:  /mdsbulk?id=
    Retrieves one document for each id specified. Returns
    a string containing id=document body followed by the specified
    delimiter.    
    
    delim = Asci number for delimiter desired to be used between documents. 
       defaults to 13 if not specified.  Change this is if ASC(13) or CR is
       embedded in documents.
       
    Response:
      200 if any of the documents requested are found.
      404 if non of the requested documents are located

​    



### TODO: Bulk Save Multiple Documents in single request
    This handler allows multiple documents to be saved in a single
    HTTP POST operation allowing faster update of the repository.
    HTTP Verb:  POST
    URI:  /_bulk&delim=13
      delim = line delimintor used to separate records.
              specified as a decimal number of a ASCII 
              #.   EG:  9 = TAB, 13 = CR,  12=FF
      When not specified delim defaults to 13
    
    Post Body is set of body lines terminated with a delim which 
    defaults to a ASCI 13.
    
    Each line contains is a format of id=BODY followed by delim.
    
    Limit: Do not post a total set of body exceeding 15Mb in any single post. This limit will be lower when using a load balancer between the client and the server.  
    
    save multiple example


### TODO: Bulk delete all documents starting with Prefix
    ..
    ..

### TODO: Healthcheck

```
..
..
```

## 

## Performance Metrics

### Larger Server with 12 3.9 Ghtz Cores

> [A 12 core I7 Xeon X5670 2.93 GHTZ Hex core server with 96GB ram and 240 GB SSD drive (Package-E)](https://www.cloudsouth.com/dedicated-servers/)  When reading from and writing to SSD 9,387 inserts per second or 16,575 reads per second when reading and writing documents averaging 1K each.    
>
> When Reading and Writing from Barracuda drive in Raid 0 configuration 22,066 reads per second or 1.324 million reads per minutes.     When writing to the Barracuda drive 619 updates per second using single record updates.  The CPU remained around 5% most of the insert run and about 

### Small server  Dell XPS 13 with 2 cores

>
> Measured performance for this service is sustained 5,900 single record requests per second on a 2 (2.2 Ghtz Core) I7 class server with 16GB Ram running windows 10 with reasonable cache hit rates.  It averages 3900 single 1K record reads per second with nearly 100% cache miss rate.   Averages 1,900 inserts per second with single record requests.   At 3,900 RPS the disk utilization is over 95% (261 MB/s or 2.08 Gbs)  with total CPU utilization at 99% with MDS  CPU utilization at 72% for MDS and test client taking 12%.   At 5,900 reads per second it yields  354,000 reads per hour which would allow 4 nodes with a total of 8 cores to support 1 million reads per hour.  Note:  Multi-record reads are faster than single record reads.
>
> Perfmon on Dell XPS 13 during insert ![perfmon on Dell XPS 13 during insert](documentation/perfmon-dell-xps-19-during-EXLoadPhysicians.jpg)

> > [40 MB/s =](https://toolstud.io/data/bandwidth.php?compare=network&speed=40&speed_unit=MB%2Fs) 0.4GB/s or less than 1/2 SATA 2 speed.   See [Conversion chart](http://www.wu.ece.ufl.edu/links/dataRate/DataMeasurementChart.html)
> >

> 



    ...
    ...

## What is this repository for? ##

* Quick summary:  Http service to store and retrieve metadata.
* Version: 0.01
* (https://bitbucket.org/tutorials/markdowndemo)
* [TODO Actions roadmap](documentation/todo-actions.md)

## How do I get set up? ##

* Clone the repository:


    https://bitbucket.org/joexdobs/gmds
    Set GOPATH = the directory where you downloaded the repository

* Make the executable
    ```
    go build mds.go
    ```
       Should produce mds.exe on windows or a exectuable file 
       labeled mds on linux    

       See Also:  [makego.bat](makego.bat) which sets the GOPATH env  var and then runs the build.

    Start the server from shell or cmd:

    ```
    ​```
    $ mds
    ​```
    ```

    ​

* Download Sample Physicians Data
  * This data is made available via DropBox to save space and update times for Git.  See [physicians.json.lines.src.md](data/sample/physicians.json.lines.src.md)  This must be completed before attempting to run [EXLoadPhysicians](src/EXLoadPhysicians.go)

  * Convert Sample Physicians data into list of unique ID for Read Performance Tests 

  ```
  Insert Physicians data into MDS

  go run src/EXConvertPhysiciansToIDList.go 
       Also creates a list of ID in separate file to use 
       during Read Performance Tests 

  ```

* ​

  ## Configuration

  - Default delimiter
  - Turning on Compression
  - Installing a SSL server key
  - ​

* Dependencies

    * [Go Language 1.7 or newer](https://golang.org/dl/)
    * Tested on Windows 10 
    * Will Test on Centos 64 bit

* Database configuration

    * Most data is stored in flat files on disk.   By default data is stored in the ./data/mds relative to the current directory when the [mds](src/mds.go) executable is started.  This can be changed in the config file using the setting  MDSDataDir

* Deployment instructions

## Contribution guidelines ##

* See:  [todo-actions.md](todo-actions.md)  A list of feature / design work outstanding for the project
* See: [src/GenericHTTPTestClient.md](src/GenericHTTPTestClient.md) for additional work items specific to the generic HTTP web service stress tester.
* Please talk to us before writing enhancements so we can guide the work and maximize the probability we will accept your pull requests.   Create an issue with your idea for the enhancement you wish to write and we will respond.    You can [contact me via linked in](https://www.linkedin.com/in/joe-ellsworth-68222/).

## Who do I talk to? ##

* Repo owner or admin

  * Joseph Ellsworth
    http://BayesAnalytic.com
    https://www.linkedin.com/in/joe-ellsworth-68222/

    ​

* Other community or team contact

  * Please leave a Issue note on this repository

    ​


* ​

## Design Notes: ##

This service depends heavily on the operating system file cache for performance.   This is an explicit decision that maximizes some performance advantages of Linux servers while minimizing memory consumed by the service itself.    It will perform best on locally mounted disk drives or Raid arrays.   It will perform best on servers with plenty of free RAM.   Do not run it against network drives or any other device where file caching is not natively supported by the OS. 



* Each document is stored as a file on disk.
* Files will optionally be compressed on disk based on configuration values.
* Files optionally encrypted on disk based on configuration values
* ​




## Possible libraries for extended functionality

* [btree isam in golang](https://github.com/timtadh/fs2) with support for variable length keys.  Also has some extra data structures like a memory mapped list.
* [Level DB / key value database](https://github.com/syndtr/goleveldb)
* [Go Data structures](https://github.com/timtadh/data-structures)  heap, linked, list, pool, set,  tree, trie, pool, hash
* [go lang file structures](https://github.com/timtadh/file-structures) b+, block, bptree, btree, linhash, pybptree, treeinfo, replaced with [fs2](https://github.com/timtadh/fs2)
* [Bold key/value store](https://github.com/boltdb/bolt)

##Interesting reference notes:

* [What ORM have taught me:  Just learn SQL](https://news.ycombinator.com/item?id=8133835)
* [ibm research open ldap](http://www.research.ibm.com/people/d/dverma/papers/sigmetrics2001.pdf) references 140 search requests per second with 8ms average latency.  Nice performance reporting chart. 
* [open LDAP 2.4 highlights](http://www.openldap.org/pub/hyc/LDAPcon2007.pdf) claims 2005  LDAP serving 3000 reads per second concurrent with 300 writes per second with latency under 500 milliseconds on database of 150 million entries.  Claims second test sustains 22,000 queries per second concurrently with 3400 writes per second. 
* Open Ldap performance from [ldapcon 2011](https://ldapcon.org/2011/downloads/hummel-slides.pdf) - claims 4 hex core Inel  Xeon X5680 @ 3.33GHz 12 MB level 1 cache RAID Controller with 1 GB BBWC  16 local disks 143 GB 15k rpm 2,5 " with 2 x OS RAID10 10 x LDAP RAID0 4 x logging RAID0  17,000 reads per sec or 4500 reads per sec with 700 writes per second. 
* Some good performance measures for the server to track [ad server](https://social.technet.microsoft.com/Forums/windowsserver/en-US/48f50a27-82ab-4c1e-aa5a-37fb1e2e6f5b/ad-performance?forum=winserverDS)
* [LDAP auth agent for nginx](https://github.com/sepich/nginx-ldap/blob/master/README.md) claims to support 6.7K reads per second with ldap caching turned on.
* ​



# Including Markdown for a GO server 

[server side sample](http://spec.commonmark.org/dingus/?text=%23%23%20Try%20CommonMark%0A%0AYou%20can%20try%20CommonMark%20here.%20%20This%20dingus%20is%20powered%20by%0A%5Bcommonmark.js%5D(https%3A%2F%2Fgithub.com%2Fjgm%2Fcommonmark.js)%2C%20the%0AJavaScript%20reference%20implementation.%0A%0A1.%20item%20one%0A2.%20item%20two%0A%20%20%20-%20sublist%0A%20%20%20-%20sublist%0A%0A%23%20Joe%20Header%0A%20%20%20%20%20Apple%0A%20%20%20%20%20Frank%0A%20%20%20%20%20Jimbo%0A%20%20%20%20%20Killer%0A)

