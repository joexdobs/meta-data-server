export GOPATH=$PWD
#:  Run get-dependencies before running this
#:  script.
go build src/mds.go
go build src/EXREadSingleRecord.go
go build src/EXSaveSingleRecord.go
go build src/EXReadSingleMultiThreaded.go
go build src/EXDeleteSingleRecord.go
go build src/EXLoadPhysicians.go
#:: LoadPhysicians requires data downloaded separately
#::  see: data/sample/physicians.json.lines.src.md
go build src/GenericHTTPTestClient.go

