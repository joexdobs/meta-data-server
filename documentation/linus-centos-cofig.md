# Auto Configuration Linux Centos

## Basic Script 

For Centos server

```
useradd jwork 
  creates the user jwork

passwd jwork
  enter desired password for jwork

usermod -aG wheel jwork
  Add user to group capable of sudo
  User must log out and re-login before new group will be active.

Login as jwork
mkdir tmp
cd tmp

curl -LO https://storage.googleapis.com/golang/go1.7.linux-amd64.tar.gz

# unzips the GO runtime as install into Linux system directories.
sudo tar -C /usr/local -xvzf go1.7.linux-amd64.tar.gz

# Install the git client so we can pull source from 
# repository.
sudo yum -y install git

# Install unzip so we can decompress files in a future step.
sudo yum -y install unzip

# Install wget which seems to work more reliably when downloading binary
# files than curl. 
sudo yum -y install wget
  
go get github.com/golang-commonmark/markdown
go get github.com/golang-commonmark/mdtool

git clone https://bitbucket.org/joexdobs/meta-data-server.git

cd meta-data-server/data/sample

curl -L -o physicians.json.lines.zip  https://www.dropbox.com/s/hof2d78sfb1c9oh/physicians.json.lines.zip?dl=1
   Note:  need to modify to save the file without the ?dl=1 at end. 

unzip  physicians.json.lines.zip?dl=1
   Note:  Claimed bad CRC but the data looked correct  May need to check Curl
       Options to ensure downloaded as binary

rm physicians.json.lines.zip?dl=1

cd ~/meta-data-server


edit .bashprofile to add  /usr/local/go/bin to path
  Find bash script to add /usr/local/go/bin to path only if go is not already in the path.

source .bash_profile

./makego.sh
   Should produce executable files mds  EXLoadPhysicians EXReadSingleMultiThreaded

Open new shell and run ./mds

Open new shell and run ./EXLoadphysicians

After it finishes then run ./EXReadSingleMultiThreaded

# Install disk bandwidth monitoring
sudo yum install iotop

# Install Network bandwidth monitoring
sudo yum -y install gcc
wget http://humdi.net/vnstat/vnstat-1.13.tar.gz
tar -xvf vnstat-1.13.tar.gz
cd vnstat-1.13/
sudo make install
sudo mkdir /var/lib/vnstat
vnstat --create -i eth0

# start mds server so it will keep running after disconnect
nohup ./mds &
Open browser with http://demo.bayesanalytic.com:9601/mds/1831566389


 
```

## Markdown

* [Render Markdown files in Chrome](https://chrome.google.com/webstore/detail/markdown-reader/gpoigdifkoadgajcincpilkjmejcaanc)
* [View markdown files directly in Chrome](https://chrome.google.com/webstore/detail/markdown-preview-plus/febilkbfcbhebfnokafefeacimjdckgl?hl=en) works but not as accurate as previous value.
* ​



## Docs



* [How to install go on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-install-go-1-7-on-centos-7)
* [Script to install GoLang on Linux](https://github.com/canha/golang-tools-install-script/blob/master/goinstall.sh) for unbutu but could be adapted for centos.
* [install program as service in centos](https://www.centos.org/forums/viewtopic.php?t=59066)
* [Managing software with Yum](https://www.centos.org/docs/5/html/yum/sn-managing-packages.html)  [Yum Command samples](https://www.centos.org/docs/5/html/5.1/Deployment_Guide/s1-yum-useful-commands.html)
* ​
* ​

## Automatic scripting configuration

- [Automating CentOS 7 configuration using shell scripts](https://unix.stackexchange.com/questions/224711/automating-centos-7-configuration-using-shell-scripts) blog with example and error handling
- [write a install script for centos](https://unix.stackexchange.com/questions/217195/writing-an-install-script-for-centos)  user group question with example
- ​

## Shell Scripting

* ​
* [Shell script to show server utilization ](https://www.tecmint.com/basic-shell-programming-part-ii/)
* ​

## Auto Configuration tools

* [Salt in 10 minutes](https://docs.saltstack.com/en/latest/topics/tutorials/walkthrough.html)
* ​



### System Admin

* [80 system admin performance monitoring tools in linux](https://www.serverdensity.com/monitor/linux/how-to/) includes both console and graphical tools.  Includes both network and system status.
* [Salt masterless quickstart](https://docs.saltstack.com/en/latest/topics/tutorials/quickstart.html) use salt to configure a single machine
* [How to install Puppet in Stand alone mode on centose 7](https://www.digitalocean.com/community/tutorials/how-to-install-puppet-in-standalone-mode-on-centos-7) 
* [How to setup chef 12 on centos 7](http://www.itzgeek.com/how-tos/linux/centos-how-tos/setup-chef-12-centos-7-rhel-7.html)
* ​





