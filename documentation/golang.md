# GO Lang Notes helpful during design

* [GO by example Tutorial](https://gobyexample.com) one of the best 2 hour investment to remember the GO language.
* [Effective GO](https://golang.org/doc/effective_go.html)   Essential guide for writing good GO.
* [Learn GO Tutorials and Introductions](https://github.com/golang/go/wiki/Learn)
* The Go Programming Language The Project Help Blog Play  Writing Web Applications
* https://golang.org/doc/articles/wiki/

- [how to write go code](https://golang.org/doc/code.html)
- [Error Handeling in GO](https://blog.golang.org/error-handling-and-go)
- [Function types in golang](http://jordanorelli.com/post/42369331748/function-types-in-go-golang)   Important context in how to thing about function pointers.  Functions with methods and closures with context for functions to make data available. 
- [Interfaces and methods](https://golang.org/doc/effective_go.html#interface_methods)  includes nice example of interfacing a http server to a channel
- [Writing module programs with go plugins](https://medium.com/learning-the-go-programming-language/writing-modular-go-programs-with-plugins-ec46381ee1a9)
- [go by example json](https://gobyexample.com/json)
- [A whirlwind tour of Go’s runtime environment variables](https://dave.cheney.net/2015/11/29/a-whirlwind-tour-of-gos-runtime-environment-variables)

**Markdown**

* [server with markdown processing built in]()
* [markdown plugin for chrome](https://chrome.google.com/webstore/detail/markdown-reader/gpoigdifkoadgajcincpilkjmejcaanc)
* [markdown sample in GO](https://github.com/golang-commonmark/mdtool)
* ​

## Logs

* [log package](https://golang.org/pkg/log/)
* [https://www.goinggo.net/2013/11/using-log-package-in-go.html](https://www.goinggo.net/2013/11/using-log-package-in-go.html)
* Add HTTP Logging compliant with weblog  [go weblog](https://github.com/lestrrat/go-apache-logformat/blob/master/logformat_test.go)  [apache web log format](http://httpd.apache.org/docs/current/logs.html) [weblog expert format](https://www.weblogexpert.com/help/wlexpert/source/log_formats.htm) [cespare log.go for apache format](https://gist.github.com/cespare/3985516)  [iis apache log format](http://surfray.com/?id=blog&news=38133)

## Strings

- [Split and Join](https://www.dotnetperls.com/split-go)  includes both normal and regex
- [Building strings using Byte buffer - efficient concatenation]()

## Files and IO

- [Writing files in GO](https://gobyexample.com/writing-files)

- [Create recursive directories in GO](https://stackoverflow.com/questions/37932551/mkdir-if-not-exists-using-golang)  [using os package](http://www.musingscafe.com/create-directory-and-nested-directory-path-in-golang/) 

- [Check file exists](http://www.musingscafe.com/check-if-a-file-or-folder-exists-in-golang/)

- #### Non Blocking File IO

  - [Streaming data in GO without bytes.Buffer](https://medium.com/stupid-gopher-tricks/streaming-data-in-go-without-buffering-3285ddd2a1e5)  uses a go channel to write to a buffer to bypass a deadlock issue.
  - [Reading a file concurrently](https://stackoverflow.com/questions/27217428/reading-a-file-concurrently-in-golang)   [sample](https://play.golang.org/p/coja1_w-fY)
  - [Non blocking channel operations](https://gobyexample.com/non-blocking-channel-operations)
  - ​

### Threading and Concurrency

* [worker thread pool](https://gobyexample.com/worker-pools)  [rate limiting](https://gobyexample.com/rate-limiting)  [timers]()  
* [golang channels tutorial](http://guzalexander.com/2013/12/06/golang-channels-tutorial.html)
* [Go channels and pipelines](https://blog.golang.org/pipelines) good example of channels to feed pipelines to fan out / consolidate.  Fan-out, fan-in Multiple functions can read from the same channel until that channel is closed; this is called fan-out. This provides a way to distribute work amongst a group of workers to parallelize CPU use and I/O.    Includes samples of computing MD5 from series of file names which is similar to what we need but does not limit the set of total file read requests for all input requests bug if we kept the GO routines between use and treated them in a Queue may work.   Probably easier just to leave it and add latter as optimization. 
* [Effective Concurrency in GO effective go](https://golang.org/doc/effective_go.html#concurrency)
* [GO Concurrency patterns and threads](https://blog.golang.org/go-concurrency-patterns-timing-out-and)
* [Concurrency is not parallelism](https://blog.golang.org/concurrency-is-not-parallelism)
* [Share memory by communicating instead of threading and lock semantics](https://blog.golang.org/share-memory-by-communicating)
* [Example Read multiple files simutaneously](https://play.golang.org/p/lWl9vmiy6O)  using go channels.    Problem is that it does not control the total number of file readers spawned.
* [Go channels used to implement a client / server](http://elliot.land/post/goroutines-and-channels-a-real-ly-simple-server-in-go)
* [How to use go routine like thread pool](https://stackoverflow.com/questions/18267460/how-to-use-a-goroutine-pool)  [sample](https://play.golang.org/p/fruJiGBWjn) uses wait group to wait for all go requests to complete.
* [go by example worker threads](https://gobyexample.com/worker-pools)  [mutex for counters across threads](https://gobyexample.com/mutexes) [stateful goroutines](https://gobyexample.com/stateful-goroutines)
* [Go Thread pool programming with priority job queue example](https://www.goinggo.net/2013/05/thread-pooling-in-go-programming.html)
* [Worker Threads buffered channels](https://golangbot.com/buffered-channels-worker-pools/) more complete job queue system than many examples.   [emulate io select](https://golangbot.com/select/)
* [https://stackoverflow.com/questions/38170852/is-this-an-idiomatic-worker-thread-pool-in-go](Idiomatic worker thread pool in go)
* [go Channels are bad and slow](http://www.jtolds.com/writing/2016/03/go-channels-are-bad-and-you-should-feel-bad/)  a alternative view of GO channels with some interesting performance #.

- [Support similar to synchronized lock](https://gobyexample.com/mutexes)

- ```
  type MyObject struct{
      Number int
      sync.Mutex
  }

  func (m *MyObject)Increment(){
      m.Lock()
      defer m.Unlock()
      m.Number++
  }
  ```

  ​

## HTTP Server & Handlers 

* [simple GO server with Markdown handler built in](http://harpjs.com/docs/development/markdown) - This seems like a good idea but there are [ markdown handlers available as plugins for Chrome](https://chrome.google.com/webstore/detail/markdown-reader/gpoigdifkoadgajcincpilkjmejcaanc) so there is not a huge benefit of building into the server unless supporting public users who can not install the plugin.

* [Make restful api in GO](https://thenewstack.io/make-a-restful-json-api-go/)

* [Writting fast cace service in go](https://allegro.tech/2016/03/writing-fast-cache-service-in-go.html)

* [Mini HTTP Server with gzip and file mod time](https://gist.github.com/alexisrobert/982674)  also includes support if modified since value

* [Example file server](https://golang.org/pkg/net/http/#example_FileServer_stripPrefix)

* [Example of using context in http server](https://blog.golang.org/context/server/server.go)

* [Dissecting golang's HandlerFunc, Handle and DefaultServeMux](http://echorand.me/dissecting-golangs-handlerfunc-handle-and-defaultservemux.html)   Good walkthrough of how GO HTTP handler dispatch works.  Includes references back to original source modules.   Example of creating server context that works through all handlers.

* [default serve mux](https://godoc.org/net/http#DefaultServeMux) includes sample servers and most of the important HTTP server documentation.  Includes sample of a server using SSL.   Also includes support for HTTP 2.0 push.

* [GoLang Context](https://golang.org/src/net/http/request.go#L316) is new way to support handlers singe version 1.8.  Provides better concurrency and timeout support. 

* [Revisiting context in golang 1.7](https://joeshaw.org/revisiting-context-and-http-handler-for-go-17/) [How to correctly using context in golang 1.7](https://medium.com/@cep21/how-to-correctly-use-context-context-in-go-1-7-8f2c0fafdf39)

* [simple go lang request context example](https://gocodecloud.com/blog/2016/11/15/simple-golang-http-request-context-example/)  I do not think this is correct because it does not use the extended types method of context transfer.  

* [GO Context as to support rapid cleanup,  request handlers, etc](https://blog.golang.org/context)   Includes a http server with a custom handler that maintains context with a timeout.   Includes samples of calling a JSON HTTP second service with proper error recovery.   Also shows how to make the outbound JSON request and then process response in secondary goroutine.

*  Avoiding global using custom handlers - https://elithrar.github.io/article/custom-handlers-avoiding-globals/

* [[Fasthttp in golang claims to be 10X faster than standard library](https://husobee.github.io/golang/fasthttp/2016/06/23/golang-fasthttp.html)   [fasthttp library](https://github.com/valyala/fasthttp)  [fileserver](https://github.com/valyala/fasthttp/blob/master/examples/fileserver/fileserver.go) includes nice example of a server environment variables as context passed to request handler. 

* Handling 1 Million Requests per Minute with Go](http://marcio.io/2015/07/handling-1-million-requests-per-minute-with-golang/)  Using go routine dispatcher gave much better scalability when interacting with AWS requests.

* [Echo http server library claims to be 10X faster than built in server](https://echo.labstack.com/guide)  Includes context based handler interface recommended by Google for highly concurrent applications.     [Easily extended context for requests for custom data structures](https://echo.labstack.com/guide/context)  uses a radix tree for dispatch with zero memory allocation.   [Example use with auto import direct from github](https://echo.labstack.com/cookbook/hello-world)  [example to auto import tls cert](https://echo.labstack.com/cookbook/auto-tls)    [file download](https://echo.labstack.com/cookbook/file-download)  [file upload](https://echo.labstack.com/cookbook/file-upload)  [streaming example](https://echo.labstack.com/cookbook/streaming-response)   [example of context variables that live for life of server](https://forum.labstack.com/t/where-to-keep-db-connection/350)

* [GO Based HTTP performance compared to nginx](https://www.reddit.com/r/golang/comments/28so0e/go_networking_performance_vs_nginx/)  claims that [copyio](https://github.com/yosssi/go-fileserver/blob/master/file_server.go#L48-L49)  is much faster than servecontent.  I think this is probably correct since it is avoiding some memory allocation  [Version file server that claims to be faster than nginx](https://golang.org/src/net/http/fs.go?s=3310:3414#L103)  The fastest backend web framework for Go. — **Iris**    The fastest full-featured web framework for Golang. Crystal clear. — **Gin Gonic**  Fast and unfancy HTTP server framework for Go (Golang). Up to 10x faster than the rest. — **Echo**

* [HTTP Tree mux router](https://github.com/dimfeld/httptreemux) good example of several concepts such as context routers

* #### Supporting HTTP chunked

* ​

* send chunked data via http](https://stackoverflow.com/questions/26769626/send-a-chunked-http-response-from-a-go-server) Seems to require calling the flush method.

* #### HTTP Client

* [Simple GO client to feat robots.txt](https://dlintw.github.io/gobyexample/public/http-client.html)

* ​


## OIDC

* [OIDC GOLang Libraries](https://golanglibs.com/top?q=oidc)
* [OIDC sample client in GO](https://github.com/coreos/go-oidc/tree/master/example)

### OIDC Server

* [Hydra OIDC auth server](https://github.com/ory/hydra)
* [DEX OIDC auth server](https://github.com/coreos/dex)
* [osi OAuth2 server library](https://github.com/RangelReale/osin) use to build your own OAuth2 authentication service
* [foulkon autho2 authorization server](https://github.com/Tecsisa/foulkon)
* [osite oAuth2 server library](https://github.com/ory/fosite)
* ​

### OIDC Client

* [OIDC sample client in GO](https://github.com/coreos/go-oidc/tree/master/example)
* [GoID open ID Client in GO](https://github.com/coreos/go-oidc)
* [Open ID OAuth2 provider](https://github.com/coreos/dex)
* [Change oidc client](https://github.com/ericchiang/oidc)
* ​

## Kafka

* [Sample Kafka Producer and Consumer Example](https://github.com/vsouza/go-kafka-example)

### Other Network

* [new chunked writer](https://golang.org/pkg/net/http/httputil/#NewChunkedWriter)   [Chunked writer](https://golang.org/src/net/http/httputil/httputil.go?s=1309:1358#L23)   Claims that http library automatically adds chunked support.  [chunked.go](https://golang.org/src/net/http/internal/chunked.go)
* Binary protocol for fast server to server communication [gobs of data](https://blog.golang.org/gobs-of-data)  includes using GO reflection to auto encode the data.
* [A distributed and coördination-free log management system](https://github.com/oklog/oklog)
* [Process manager for Procfile-based applications](https://github.com/DarthSim/hivemind)



* [Headless CMS with automatic JSON API. Featuring auto-HTTPS from Let's Encrypt, HTTP/2 Server Push, and flexible server framework written in Go.](https://github.com/ponzu-cms/ponzu)

### Markdown

* [How to render Markdown to HTML in GO](https://stackoverflow.com/questions/23124008/how-can-i-render-markdown-to-a-golang-templatehtml-or-tmpl-with-blackfriday)
* [Realgo markdown server in GO](https://github.com/yusukebe/revealgo) Does the markdown processing in Javascript
* [Simple HTTP server with markdown rendering](https://github.com/renstrom/go-wiki)
* [Black Friday Mardown parser](https://github.com/russross/blackfriday)
* [Common Mark Markdown parser](https://github.com/golang-commonmark/markdown) [package docs](https://godoc.org/github.com/golang-commonmark/markdown)
* [Reference implementation of Markdown](https://github.com/commonmark/CommonMark)
* [How to code a Markdown logging system in GO](http://blog.will3942.com/creating-blog-go)
* [Markdown fmt](https://github.com/shurcooL/markdownfmt)  similar to gofmt for for markdown
* ​

